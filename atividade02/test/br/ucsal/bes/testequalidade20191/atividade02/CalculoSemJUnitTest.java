package br.ucsal.bes.testequalidade20191.atividade02;

//NUNCA FA�A ASSIM!
public class CalculoSemJUnitTest {

	public static void main(String[] args) {
		CalculoSemJUnitTest calculoTest = new CalculoSemJUnitTest();
		calculoTest.testarFatorial5();
	}

	public void testarFatorial5() {
		// Setup

		// Definir dados de entrada
		Integer n = 5;

		// Definir a sa�da esperada
		Long fatorialEsperado = 120L;

		// Executar o m�todo que ser� testado e obter o resultado atual
		Long fatorialAtual = Calculo.calcularFatorial(n);

		// Comparar o resultado esperado com o resultado atual
		if (fatorialEsperado.equals(fatorialAtual)) {
			System.out.println("Teste testarFatorial5 ok");
		} else {
			System.out.println(
					"Erro no teste testarFatorial5, esperado=" + fatorialEsperado + "; atual=" + fatorialAtual);
		}
	}

}
