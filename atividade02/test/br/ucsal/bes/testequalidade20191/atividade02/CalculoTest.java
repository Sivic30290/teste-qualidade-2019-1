package br.ucsal.bes.testequalidade20191.atividade02;

import org.junit.Assert;
import org.junit.Test;

public class CalculoTest {
	
	@Test
	public void testarFatorial5() {
		// Setup

		// Definir dados de entrada
		Integer n = 5;

		// Definir a sa�da esperada
		Long fatorialEsperado = 120L;

		// Executar o m�todo que ser� testado e obter o resultado atual
		Long fatorialAtual = Calculo.calcularFatorial(n);

		// Comparar o resultado esperado com o resultado atual
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}
}
