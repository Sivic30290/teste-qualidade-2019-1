package br.ucsal.bes.testequalidade20191.aula07.domain;

public class Aluno {

	private Integer matricula;

	private String nome;

	private SituacaoEnum situacao;

	private String email;

	private Integer anoNascimento;

	public Aluno() {

	}

	public Aluno(Integer matricula, String nome, SituacaoEnum situacao, String email, Integer anoNascimento) {
		super();
		this.matricula = matricula;
		this.nome = nome;
		this.situacao = situacao;
		this.email = email;
		this.anoNascimento = anoNascimento;
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public SituacaoEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoEnum situacao) {
		this.situacao = situacao;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getAnoNascimento() {
		return anoNascimento;
	}

	public void setAnoNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
	}

}
