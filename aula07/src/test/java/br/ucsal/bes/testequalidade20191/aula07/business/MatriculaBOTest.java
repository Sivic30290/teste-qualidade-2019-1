package br.ucsal.bes.testequalidade20191.aula07.business;

import org.junit.Test;

import br.ucsal.bes.testequalidade20191.aula07.builder.AlunoBuilder;
import br.ucsal.bes.testequalidade20191.aula07.builder.AlunoObjectMother;
import br.ucsal.bes.testequalidade20191.aula07.domain.Aluno;

public class MatriculaBOTest {

	@Test
	public void matricularAlunosTrancados() {
		Aluno aluno1 = AlunoObjectMother.umAlunoTrancadoMaiorDeIdade();
		Aluno aluno2 = AlunoBuilder.umAluno().comAnoNascimento(2000).comMatricula(1990).comMatriculaTrancada().build();
	}

	@Test
	public void matricularAlunosCancelado() {

	}

	@Test
	public void matricularAlunosMenorIdade() {

	}

}
