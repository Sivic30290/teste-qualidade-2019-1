package br.ucsal.bes.testequalidade20191.aula14selenium;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class SiteUcsalTest {

	private static WebDriver driver;

	// @BeforeClass
	public static void setupChrome() {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\antoniocp\\Documents\\2019-1\\teste-qualidade\\workspace\\aula14seleinium\\lib\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	@BeforeClass
	public static void setupFirefox() {
		System.setProperty("webdriver.gecko.driver", "C:\\Users\\antoniocp\\Documents\\2019-1\\teste-qualidade\\workspace\\aula14seleinium\\lib\\geckodriver.exe");
		System.setProperty("webdriver.firefox.bin", "C:\\Program Files (x86)\\Mozilla Firefox\\Firefox.exe");
		DesiredCapabilities capabilities = DesiredCapabilities.firefox();
		capabilities.setCapability("marionette", true);
		driver = (WebDriver) new FirefoxDriver(capabilities);
	}

	@Test
	public void testeUCSal01() {

		// Abrir p�gina da UCSal
		driver.get("http://www.ucsal.br");

		// Clicar no link Gradua��o
		WebElement graduacaoLink = driver.findElement(By.linkText("GRADUA��O"));
		graduacaoLink.click();

		// Preencher o input de pesquisa
		WebElement queryInput = driver.findElement(By.name("query"));
		queryInput.sendKeys("teste qualidade");

		// Submeter o form:
		queryInput.submit();

		// Obter o conte�do da p�gina
		String conteudo = driver.getPageSource();

		// Verificar se o texto �Teste Qualidade CST 2016-2� est� na p�gina
		Assert.assertTrue(conteudo.contains("Teste Qualidade CST 2016-2"));

	}

	@AfterClass
	public static void teardown() {
		driver.quit();
	}

}
