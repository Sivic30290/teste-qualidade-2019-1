package br.ucsal.bes20191.testequalidade.restaurante.poolista01.implementacaopropria;

import br.ucsal.bes20191.testequalidade.restaurante.poolista01.FatorialUtil;

public class FatorialUtilStub extends FatorialUtil {

	@Override
	public Long calcularFatorial(int n) {
		switch (n) {
		case 0:
			return 1L;
		case 1:
			return 1L;
		}
		return null;
	}

}
