package br.ucsal.bes20191.testequalidade.restaurante.poolista01.implementacaopropria;

import java.util.HashMap;
import java.util.Map;

import br.ucsal.bes20191.testequalidade.restaurante.poolista01.FatorialUtil;

public class FatorialUtilMock extends FatorialUtil {

	private Map<String, Integer> chamadas = new HashMap<>();

	@Override
	public Long calcularFatorial(int n) {
		registrar("calcularFatorial", n);
		switch (n) {
		case 0:
			return 1L;
		case 1:
			return 1L;
		}
		return null;
	}

	private void registrar(String metodo, int n) {
		String identificacao = gerarIdentificadorMetodo(metodo, n);
		if (!chamadas.containsKey(identificacao)) {
			chamadas.put(identificacao, 1);
		} else {
			chamadas.put(identificacao, chamadas.get(identificacao) + 1);
		}
	}

	private String gerarIdentificadorMetodo(String metodo, int n) {
		return metodo + "(" + n + ")";
	}

	public void verificar(String metodo, Integer parametro, Integer qtdOcorrencias) {
		String identificacao = gerarIdentificadorMetodo(metodo, parametro);
		if (!chamadas.containsKey(identificacao) || chamadas.get(identificacao) != qtdOcorrencias) {
			throw new RuntimeException(
					"A chamada ao m�todo " + identificacao + " n�o ocorreu " + qtdOcorrencias + " vez(es).");
		}
	}

}
