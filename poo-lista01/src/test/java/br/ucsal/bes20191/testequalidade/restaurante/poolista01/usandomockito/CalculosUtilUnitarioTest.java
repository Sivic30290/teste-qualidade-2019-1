package br.ucsal.bes20191.testequalidade.restaurante.poolista01.usandomockito;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import br.ucsal.bes20191.testequalidade.restaurante.poolista01.CalculosUtil;
import br.ucsal.bes20191.testequalidade.restaurante.poolista01.FatorialUtil;

public class CalculosUtilUnitarioTest {

	private CalculosUtil calculosUtil;

	private FatorialUtil fatorialUtilMock;

	@Before
	public void setup() {

		// A chamada ao Mockito.mock(classe) n�o � a mais interessante!
		// O ideal � utilizar o initMocks ou, ainda melhor, MockitoJUnitRunner.
		// � poss�vel ainda utilizar Rules para garantir a verifica��o do uso do
		// Mockito.
		fatorialUtilMock = Mockito.mock(FatorialUtil.class);

		Mockito.when(fatorialUtilMock.calcularFatorial(0)).thenReturn(1L);
		Mockito.when(fatorialUtilMock.calcularFatorial(1)).thenReturn(1L);

		calculosUtil = new CalculosUtil(fatorialUtilMock);
	}

	/**
	 * Verificar c�lculo do valor de E. Caso de teste: entrada n=1; sa�da
	 * esperada=2
	 */
	@Test
	public void testarCalculoE1() {
		// Defini��o da entrada
		Integer n = 1;

		// Defini��o da sa�da esperada
		Double eEsperado = 2d;

		// Execu��o do m�todo sobre teste e obten��o do resultado esperado
		Double eAtual = calculosUtil.calcularE(n);

		// Compara��o do resultado esperado com o resultado atual
		Assert.assertEquals(eEsperado, eAtual, 0.0001);

		// Verifica��o de chamadas. Neste exemplo, pelo m�todo calcularE(n) se
		// tratar de um m�todo query, n�o seria necess�rio fazer esta
		// verifica��o!
		Mockito.verify(fatorialUtilMock).calcularFatorial(0);
		Mockito.verify(fatorialUtilMock).calcularFatorial(1);

		// A chamada abaixo falha, pois n�o houve chamada ao calcularFatorial(5)
		// Mockito.verify(fatorialUtilMock).calcularFatorial(5);
	}

}
