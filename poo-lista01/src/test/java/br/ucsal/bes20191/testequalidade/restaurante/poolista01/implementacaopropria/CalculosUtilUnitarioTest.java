package br.ucsal.bes20191.testequalidade.restaurante.poolista01.implementacaopropria;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.ucsal.bes20191.testequalidade.restaurante.poolista01.CalculosUtil;
import br.ucsal.bes20191.testequalidade.restaurante.poolista01.FatorialUtil;

public class CalculosUtilUnitarioTest {

	private CalculosUtil calculosUtil;

	private FatorialUtil fatorialUtil;

	@Before
	public void setup() {
		// Para garantir a independ�ncia do teste unit�rio, precisamos
		// substituir a implementa��o de FatorialUtil por um dubl�.
		// Respostas esperadas pra o dubl� (stub):
		// calcularFatorial(0)=1
		// calcularFatorial(1)=1
		// Instancia original: FatorialUtil fatorialUtil = new FatorialUtil();

		// O uso do stub consegue oferecer o isolamento.
		// fatorialUtil = new FatorialUtilStub();

		// O uso do mock vai permitir a rastreabilidade da chamada.
		fatorialUtil = new FatorialUtilMock();

		calculosUtil = new CalculosUtil(fatorialUtil);
	}

	/**
	 * Verificar c�lculo do valor de E. Caso de teste: entrada n=1; sa�da
	 * esperada=2
	 */
	@Test
	public void testarCalculoE1() {
		// Defini��o da entrada
		Integer n = 1;

		// Defini��o da sa�da esperada
		Double eEsperado = 2d;

		// Execu��o do m�todo sobre teste e obten��o do resultado esperado
		Double eAtual = calculosUtil.calcularE(n);

		// Compara��o do resultado esperado com o resultado atual
		Assert.assertEquals(eEsperado, eAtual, 0.0001);

		// Verifica��o de chamadas. Neste exemplo, pelo m�todo calcularE(n) se
		// tratar de um m�todo query, n�o seria necess�rio fazer esta
		// verifica��o!
		((FatorialUtilMock) fatorialUtil).verificar("calcularFatorial", 0, 1);
		((FatorialUtilMock) fatorialUtil).verificar("calcularFatorial", 1, 1);

		// A verifica��o abaixo falha, pois n�o foi feita nenhuma chamada ao
		// calcularFatorial(2)
		// ((FatorialUtilMock) fatorialUtil).verificar("calcularFatorial", 2,
		// 1);
	}

}
