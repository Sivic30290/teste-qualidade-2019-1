package br.ucsal.bes20191.testequalidade.restaurante.enums;

public enum SituacaoMesaEnum {
	LIVRE, OCUPADA;
}
