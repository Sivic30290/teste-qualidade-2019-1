package br.ucsal.bes.testequalidade20191.atividade02mvn;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class CalculoParametrizadoTest {

	@Parameters(name = "testarFatorial{0}")
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] { { 0, 1L }, { 1, 1L }, { 2, 2L }, { 3, 6L }, { 4, 24L }, { 5, 120L },
				{ 6, 720L }, { 7, 5040L }, { 8, 40320L }, { 9, 362880L } });
	}

	@Parameter // default = (0)
	public Integer n;

	@Parameter(1)
	public Long fatorialEsperado;

	@Test
	public void testarFatorial() {
		// Executar o m�todo que ser� testado e obter o resultado atual
		Long fatorialAtual = Calculo.calcularFatorial(n);

		// Comparar o resultado esperado com o resultado atual
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}
}
